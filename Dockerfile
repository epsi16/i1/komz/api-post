FROM node:10-alpine

RUN mkdir -p /home/node/api-post/node_modules && chown -R node:node /home/node/api-post

WORKDIR /home/node/api-post

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 8080

CMD [ "node", "bin/www" ]
