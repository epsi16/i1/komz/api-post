# api-post

## Installation
- `cp docker-compose.yml.dist docker-compose.yml`
  - Ajouter vos `POSTGRESQL_HOST`, `POSTGRESQL_USER`, `POSTGRESQL_PASSWORD` et `POSTGRESQL_PORT`  
- `cp .env.dist .env`
    - Ajouter vos `POSTGRESQL_HOST`, `POSTGRESQL_USER`, `POSTGRESQL_PASSWORD` et `POSTGRESQL_PORT`
- `make install`

## Démarrage
- `make start`

## Arrêt
- `make stop`

## Supprimer le conteneur et ses images
- `make clean`

## Consulter les logs
- `make logs`

## Réccupérer l'IP du conteneur
- `make ip`
