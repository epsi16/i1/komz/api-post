var express = require('express');
const jwt = require("jsonwebtoken");
const config = require("../config");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const { v4: uuidv4 } = require('uuid');
var router = express.Router();
const axios = require("axios");
const { Pool, Client } = require("pg");
var FormData = require('form-data');


/* GET home page. */
const pool = new Pool({
  user: config.POSTGRESQL_USER,
  host: config.POSTGRESQL_HOST,
  database: "api-post",
  password: config.POSTGRESQL_PASSWORD,
  port: config.POSTGRESQL_PORT
});

router.get('/post/bdd', (req, res) => {
  console.log(config.POSTGRESQL_USER);
  console.log(config.POSTGRESQL_HOST);
  console.log(config.POSTGRESQL_PASSWORD);
  console.log(config.POSTGRESQL_PORT);
  console.log("[GET] /post/bdd");

  res.header('Access-Control-Allow-Origin', '*');

  console.log(pool.connect());

  res.send("ok");
});


router.get('/post/:id/:id_current_user', function(req, res, next) {
  console.log("[GET] /post/" + req.params.id + "/" + req.params.id_current_user);
  res.header('Access-Control-Allow-Origin', '*');
  pool.query("SELECT *,(SELECT count(*) FROM PUBLIC.post WHERE id_parent = $1) AS count_retweet  from PUBLIC.post p WHERE id = $1" ,[req.params.id], (error, result) => {
    console.log(error);
    if(result.rows.length !==1){
      res.status(400).send(error);
    }else{
      axios.get(config.API_USER_URL + result.rows[0].username_user).then(response =>{
        obj = new Object();
        obj.added = result.rows[0].added;
        obj.boolean_parent = result.rows[0].boolean_parent;
        obj.content = result.rows[0].content;
        obj.id = result.rows[0].id;
        obj.id_parent = result.rows[0].id_parent;
        obj.link_image = result.rows[0].link_image;
        obj.link_image_profile = response.data.link_image_profile;
        obj.username_user = result.rows[0].username_user;
        obj.updated = result.rows[0].updated;
        obj.nb_like = 222;
        obj.nb_comment = 33;
        obj.nb_retweet = parseInt(result.rows[0].count_retweet);
        axios.get(config.API_LIKE_URL + 'post_' + req.params.id).then(response => {
          obj.nb_like = response.data;
          axios.get(config.API_COMMENT_URL + req.params.id).then(repppp => {
            obj.nb_comment = repppp.data;
            axios.get(config.API_LIKE_URL + 'byUser/' + req.params.id_current_user + '/post/' + req.params.id).then(rep => {
              obj.isLike = rep.data;
              res.status(200).send(obj)
            });
          });
        });
      })
    }
  });
});


router.get('/post/feed/:id/:first/:number', async function(req, res, next) {
  console.log("[GET] /post/feed/" + req.params.id + "/" + req.params.first + "/" + req.params.number);
  res.header('Access-Control-Allow-Origin', '*');
  axios.get(config.API_USER_URL + 'subscribeto/' + req.params.id + '/0/1000000000000000').then(response =>{
    response.data;
    let array_username = [];
    response.data.forEach(function (el){
      array_username.push(el.username)
    })
   var userJoin= "'" + array_username.join("','") + "'";
   var params = [req.params.number, req.params.first];
   let resultQuerry;
   var str = "SELECT *,(SELECT count(*) FROM PUBLIC.post WHERE id_parent = p.id) AS count_retweet from public.post p WHERE username_user IN (" + userJoin + ") ORDER BY added DESC LIMIT $1 OFFSET $2";
   new Promise((resolve, reject) => {
     pool.query(str, params, (error, result) => {
       console.log(error);
       resolve(result)
     });
   }).then((result) => {
     resultQuerry = result;
     if (resultQuerry.rows.length <= 0) {
       res.status(401).send("error");
     } else {
       let arrayResponse = [];
       var obj;
       Promise.all(resultQuerry.rows.map(function (el){
         obj = new Object();
         obj.added = el.added;
         obj.boolean_parent = el.boolean_parent;
         obj.content = el.content;
         obj.id = el.id;
         obj.id_parent = el.id_parent;
         obj.link_image = el.link_image;
         obj.username_user = el.username_user;
         obj.updated = el.updated;
         obj.nb_like = 222;
         obj.isLike = false;
         obj.nb_comment = 33;
         obj.nb_retweet = parseInt(el.count_retweet);
         arrayResponse.push(obj);
         return axios.get(config.API_USER_URL + el.username_user);
       })).then(responseAll =>{
         Promise.all(arrayResponse.map(async function (el) {
          return axios.get(config.API_LIKE_URL + 'post_' + el.id).then(response => {
             el.nb_like = response.data;
             responseAll.map(function (respEl) {
               if (respEl.data.username == el.username_user) {
                 el.link_image_profile = respEl.data.link_image_profile;
               }
             })
           })
         })).then(() => {
           Promise.all(arrayResponse.map(async function (el) {
             return axios.get(config.API_COMMENT_URL + el.id).then(repppp => {
               el.nb_comment = repppp.data;
             })
           })).then(() => {
             Promise.all(arrayResponse.map(async function (el) {
               return axios.get( config.API_LIKE_URL +'byUser/' + req.params.id + '/post/' + el.id).then(rep => {
                 el.isLike = rep.data;
               })
             })).then(async () => {
               for(let el of arrayResponse) {
                 if(el.id_parent !== null){
                   await axios.get(config.API_POST_URL + el.id_parent + "/" + req.params.id).then(resp => {
                     el.parentContent = resp.data;
                   }).catch(() =>{
                     el.parentContent = null;
                   });
                 }else{
                   el.parentContent = null;
                 }
               }
               res.status(200).send(arrayResponse);
             });
           });
         });
       });
     };
   });
 }).catch((error) =>{
   res.status(404).send(error);
  })
});

router.get('/post/profile/:username/:current_id_user/:first/:number', async (req, res, next) => {
  console.log("[GET] /post/profile/" + req.params.username + "/" + req.params.current_id_user + "/" + req.params.first + "/" + req.params.number);
  res.header('Access-Control-Allow-Origin', '*');
  var params = [req.params.username, req.params.number, req.params.first];
  let resultQuerry;
  var str = "SELECT *,(SELECT count(*) FROM PUBLIC.post WHERE id_parent = p.id) AS count_retweet from public.post p WHERE username_user=$1 ORDER BY added DESC LIMIT $2 OFFSET $3";
  new Promise((resolve, reject) => {
    pool.query(str, params, (error, result) => {
      console.log(error);
      resolve(result)
    });
  }).then((result) => {
    resultQuerry = result;
    var obj;
    var arrayResponse = [];
    Promise.all(resultQuerry.rows.map(function (el) {
      obj = new Object();
      obj.added = el.added;
      obj.boolean_parent = el.boolean_parent;
      obj.content = el.content;
      obj.id = el.id;
      obj.id_parent = el.id_parent;
      obj.link_image = el.link_image;
      obj.username_user = el.username_user;
      obj.updated = el.updated;
      obj.nb_like = 222;
      obj.isLike = false;
      obj.nb_comment = 33;
      obj.nb_retweet = parseInt(el.count_retweet);
      arrayResponse.push(obj);
    })).then(responseAll => {
      Promise.all(arrayResponse.map(async function (el) {
        return axios.get(config.API_COMMENT_URL + el.id).then(repppp => {
          el.nb_comment = repppp.data;
        })
      })).then(() => {
        Promise.all(arrayResponse.map(async function (el) {
          return axios.get( config.API_LIKE_URL+ 'post_' + el.id).then(response => {
            el.nb_like = response.data;
          })
        })).then(() => {
          Promise.all(arrayResponse.map(async function (el) {
            return axios.get(config.API_LIKE_URL + 'byUser/' + req.params.current_id_user + '/post/' + el.id).then(rep => {
              el.isLike = rep.data;
            })
          })).then(async () => {
            for(let el of arrayResponse) {
              if(el.id_parent !== null){
                await axios.get(config.API_POST_URL + el.id_parent + "/" + req.params.current_id_user).then(resp => {
                  el.parentContent = resp.data;
                }).catch(() =>{
                  el.parentContent = null;
                });
              }else{
                el.parentContent = null;
              }
            }
            res.status(200).send(arrayResponse);
          });
        });
      });
    });
  });
});

router.post('/post', async(req, res) => {
  console.log("[POST] /post");
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'OPTIONS,POST');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  var bool_image = false;
  if(req.files != null){
    bool_image = true;
  }

  const content = req.body.content;
  const  id_parent = req.body.id_parent;
  const username_user = req.body.username_user;
  const added  = req.body.added;

  let uuidValid = false;
  let uuid = uuidv4();

  while (!uuidValid) {
    await new Promise((resolve, reject) => {
      pool.query('SELECT COUNT(*) FROM PUBLIC.post WHERE id = $1', [uuid], (error, result) => {
        console.log(error);
        resolve(result);
      });
    }).then((result) => {
      uuidValid = (result.rows[0].count == 0) ? true : false;
      uuid = (uuidValid == true) ? uuid : uuidv4();
    });
  }

  if (bool_image == true){
    const { headers, files } = req;
    const { data, name } = files.image;
    var bodyFormData = new FormData();
    bodyFormData.append('name', 'post-' + uuid);
    bodyFormData.append('content', Buffer.from(data),name);

    axios.post(config.API_IMAGE_URL, bodyFormData, {
      headers: bodyFormData.getHeaders()
    }).then(response =>{
      let link_image = response.data.url
      let boolean_parent = (id_parent == "null") ? false : true;

      let queryPOST = 'INSERT INTO PUBLIC.post (id, boolean_parent, content, link_image, id_parent, username_user, added, updated) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)';
      let paramsPOST = [uuid, boolean_parent, content == "null" ? null : content, link_image, id_parent == "null" ? null : id_parent, username_user, added, added];

      pool.query(queryPOST, paramsPOST, (error, result) => {
        if (error) {
          console.log(error)
          res.status(400).send({error: "An error has been thrown during the insert of the post"});
        } else {
          if(result.rowCount == 0){
            res.status(404).send({error: "The insert of the post has failed"});
          }else{
            console.log(res);
            res.status(200).send({message: "Post has been created"});
          }
        }
      })

    });
  }else{
    let boolean_parent = (id_parent == "null") ? false : true;

    let queryPOST = 'INSERT INTO PUBLIC.post (id, boolean_parent, content, link_image, id_parent, username_user, added, updated) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)';
    let paramsPOST = [uuid, boolean_parent, content == "null" ? null : content, null, (id_parent == "null") ? null : id_parent, username_user, added, added];

    pool.query(queryPOST, paramsPOST, (error, result) => {
      if (error) {
        console.log(error)
        res.status(400).send({error: "An error has been thrown during the insert of the post"});
      } else {
        if(result.rowCount == 0){
          res.status(404).send({error: "The insert of the post has failed"});
        }else{
          console.log(res);
          res.status(200).send({message: "Post has been created"});
        }
      }
    })
  }

});


router.put('/post', (req, res) => {
  console.log("[PUT] /post");
  res.header('Access-Control-Allow-Origin', '*');

  const { id, content, link_image, updated } = req.body;

  let queryPUT = 'UPDATE PUBLIC.post SET content = $1, link_image = $2, updated = $3 WHERE id = $4';
  let paramsPUT = [content, link_image, updated, id];

  pool.query(queryPUT, paramsPUT, (error, result) => {
    console.log(error);
    if(error){
      res.status(400).send({ error: "An error has been thrown during the update of the post"});
    }else{
      if(result.rowCount === 0){
        res.status(404).send({error: "The update of the post has failed"});
      }else {
        res.status(200).send({message: "Post has been updated"});
      }
    }
  })
});

router.delete('/post/:id', function (req,res,next){
  console.log("[DELETE] /post/" + req.params.id);
  res.header('Access-Control-Allow-Origin', '*');
  console.log(req.params.id);


  pool.query("Delete from post WHERE id = $1" ,[req.params.id], (error, result) => {
    console.log(error);
    if(error){
      res.status(400).send({ error: "An error has been thrown during the delete of the post"});
    }else{
      if(result.rowCount === 0){
        res.status(404).send({error: "The delete of the post has failed"});
      }else {
        res.status(200).send({message: "Post has been deleted"});
      }
    }
  });
})

module.exports = router;
