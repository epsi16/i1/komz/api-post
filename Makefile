install:
	docker build -t aloismoreau/komz-api-post . && \
    	docker-compose build && \
    	npm install
start:
	docker-compose up -d
stop:
	docker-compose down
logs:
	docker-compose logs -f api-post
ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' api-post

clean:
	docker image rm komz-api-post:latest node:10-alpine aloismoreau/komz-api-post:latest
