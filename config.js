// config.js

module.exports = {
    'POSTGRESQL_HOST': process.env.POSTGRESQL_HOST,
    'POSTGRESQL_USER': process.env.POSTGRESQL_USER,
    'POSTGRESQL_PASSWORD': process.env.POSTGRESQL_PASSWORD,
    'POSTGRESQL_DATABASE': process.env.POSTGRESQL_DATABASE,
    'POSTGRESQL_PORT': process.env.POSTGRESQL_PORT,
    'API_USER_URL': process.env.API_USER_URL,
    'API_LIKE_URL': process.env.API_LIKE_URL,
    'API_COMMENT_URL': process.env.API_COMMENT_URL,
    'API_IMAGE_URL': process.env.API_PHOTO_URL,
    'API_POST_URL': process.env.API_POST_URL,
};
